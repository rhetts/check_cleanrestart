<#
.Synopsis
Check-CleanRestart.ps1
.DESCRIPTION
Powershell script for use under the Task Manager, setting "on startup", to check to see if the last boot was a clean boot or not.  Alerts team's troubleshooting queue if it was not.
.EXAMPLE
Task Manager -> New Startup Task -> Run Powershell -file {Path}\Check-CleanRestart.ps1
.INPUTS
Check-CleanRestart.ps1 -Verbose -> Shows verbose logging
.OUTPUTS
Logs found under $LogFile and if unexpected shutdown, email set to team's queue.
.FUNCTIONALITY
See discription.
#>
####################### Variables for script #######################
[CmdletBinding()]
Param(
)

$LogPath = "C:\Scripts\Logs\";
Write-Verbose -Message "Setting the log path to Drive Letter:\Scripts\Logs to: $Logpath";

$LogName = "CleanRestart.log";
Write-Verbose -Message "Setting the log name: $LogName.";

$LogFile = $LogPath + $LogName;
Write-Verbose -Message "Setting the log file location and name to: $LogFile";

Write-Verbose -Message "Setting Blank Line variable.";
$BlankLine = ' ';
# Write-Verbose -Message $BlankLine;

$TodaysDate = (Get-Date).ToLongDateString();
Write-Verbose -Message "Getting todays date.  $TodaysDate";

$ServersName = $env:computername;
Write-Verbose -Message "Getting the servers name and setting a variable for use later in the script.";

$ServerName = $ServersName.ToLower();
Write-Verbose -Message "Setting the servers name, in all lower case letters, to: $ServerName.";

$EmailFrom = "no-reply+$ServerName@pdx.edu";
Write-Verbose -Message "Setting the emails FROM Field to: $EmailFrom";

$EmailTo = "alert-bas@pdx.edu";
Write-Verbose -Message "Setting the emails TO Field: $EmailTo";

$PSEmailServer = "mailhost.pdx.edu";
Write-Verbose -Message "Setting the email servers url  to: $PSEmailServer";

Write-Verbose -Message "Setting an emails subject to NULL.  This should be changed later depending on the results of the script.";
$EmailSubject = $NULL;

Write-Verbose -Message "Setting an emails body to NULL.  This should be changed later.";
$EmailBody = $NULL;

Write-Verbose -Message "Creating the variable SEvLogs to collect Event Log data and setting that variable to NULL.";
$SEvLogs = $NULL;

Write-Verbose -Message "Creating the variable RestartStatus used later in the script and setting that variable to NULL.";
$RestartStatus = $NULL;

$Eid = "20";
Write-Verbose -Message "Setting Event ID to check to: $Eid";

$NumberOfMinutes = "15"; # Number of minutes after immediate restart to look back in the logs.
# $NumberOfMinutes = "1440" # Number of minutes in a day
# $NumberOfMinutes = "10080"; # Number of minuetes in a week
# $NumberOfMinutes = "43830"; # Number of minuetes in a month
# $NumberOfMinutes = "525960"; # Number of minuetes in a year
Write-Verbose -Message "Setting the number of minutes to remove from the current date/time to: $NumberOfMinutes";

$n = 0; # each item in the array needs to be checked individually, this number allows this to happen.
Write-Verbose -Message "First entry number is set to $n. Each item in the array needs to be checked individually, this number allows this to happen.";

$Begin = (Get-Date) - (New-TimeSpan -Minutes $NumberOfMinutes);
Write-Information -Message "Start Date/Time for the HashTable to start looking at is $Begin ";

Write-Verbose -Message "Creating HashTable of the System Event Logs";
$SEvLogs = Get-WinEvent -FilterHashTable @{ LogName = "system"; StartTime = $Begin; ID = $Eid }

Write-Verbose -Message "Counting the number of log entries within the HashTable.";
$m = $SEvLogs.Count;
Write-Information -Message "Setting that number as the max for number of logs entries to check to $m.";

####################### Functions #######################

function Write-Log {
    Param(
        $Message
    )
    function TS {Get-Date -Format 'HH:mm:ss'}
    "[$(TS)]     $Message" | Tee-Object -FilePath $LogFile -Append | Write-Verbose
}


Function Get-LogStatus {
    # Checks to see if a log file exists and if not creates one
    Write-Verbose -Message "Checking to see if a log file exists and if not creates one"
	# This function requires the Write-Log Function to be run first.
    If (Test-Path $LogFile) {
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        "               $TodaysDate" | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        # Write-Log -Message $BlankLine;
        # Write-Information -Message $BlankLine;
    }
    Else {
        New-Item $LogFile -Force -ItemType File
        "               $TodaysDate" | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        # Write-Log -Message $BlankLine;
        # Write-Information -Message $BlankLine;
    }
}


Function Check-SLogs {

    Write-Verbose -Message "Starting the Message Contents Loop, checking through the HashTable's objects.";
    Write-Verbose -Message $BlankLine;
    Write-Log -Message "  Creating HashTable of the System Event Logs";
    Write-Log -Message "       Start Date/Time for the HashTable to start looking at is $Begin ";
    Write-Log -Message "       Setting that number as the max for number of logs entries to check to $m.";
    While ($n -le $m)
    {
#   "Message Contents"
    Write-Information -Message "Checking System Event Log $n (out of $m) for a status of true or false.";
    Write-Log -Message "  Checking System Event Log $n (out of $m) for a status of true or false.";
    If ($SEvLogs.Message[$n] -Match "The last shutdown's success status was true" -Or $SEvLogs.Message[$n] -Match "The last shutdown's success status was false") {
        Write-Information -Message "     System Event Log $n shows the status of the shutdown.";
        Write-Log -Message "       System Event Log $n shows the status of the shutdown.";
        
        Write-Verbose -Message "Breaking out of the Message Contents Loop.";
        Write-Verbose -Message $BlankLine;
        Break #:CheckStatus
    }
    Else {
        Write-Information -Message "     System Event Log $n did not show the status of a shutdown.";
        Write-Log -Message "       System Event Log $n did not show the status of a shutdown.";
        Write-Verbose -Message $BlankLine;
    }
    
    Write-Verbose -Message "Proceeding to the next entry in the array.";
    Write-Verbose -Message $BlankLine;
    $n++;
    }
    
#   :CheckStatus
    Write-Verbose -Message $BlankLine;
    Write-Information -Message "Checking the event message to see if it shows a status of false.";
    If ($SEvLogs.Message[$n] -Match "The last shutdown's success status was false") {
        
        Write-Verbose -Message "Event message matched on false, shutdown was unexpected.";
        Write-Log -Message "  *Warning* - The previous shutdown was unexpected.";
        Write-Warning -Message "*Warning* - The previous shutdown was unexpected.";
        Write-Verbose -Message $BlankLine;
        
        Write-Verbose -Message "Setting RestartStatus to unexpected.";
        Write-Verbose -Message $BlankLine;
        $RestartStatus = "unexpected";
        
        Write-Verbose -Message "Proceeding to sending alert email.";
        Write-Log -Message "  Proceeding to sending alert email.";
        Write-Verbose -Message $BlankLine;
        return $RestartStatus;
    
        Write-Information -Message "  Checking the event message to see if it shows a status of true.";
    }
    Elseif ($SEvLogs.Message[$n] -Match "The last shutdown's success status was true") {
        
        Write-Verbose -Message "Event message matched on true, shutdown was expected.";
        Write-Log -Message "  The previous shutdown was done properly.";
        Write-Information -Message "The previous shutdown was done properly.";
        Write-Verbose -Message $BlankLine;

        Write-Verbose -Message "Setting RestartStatus to expected.";
        Write-Verbose -Message $BlankLine;
        $RestartStatus = "expected";
        
        Write-Verbose -Message "Proceeding to sending alert email.";
        Write-Log -Message "  Proceeding to sending alert email.";
        Write-Verbose -Message $BlankLine;
        return $RestartStatus;
    }
    Else {
        Write-Warning -Message "*Warning* - Event message did not match true nor false.";
        Write-Log -Message "  The event log service was started but does not show the status of the shutdown.";
        Write-Verbose -Message $BlankLine;
        echo $RestartStatus;
    }
}


Function Send-AlertEmail {
    If ($RestartStatus -Match "unexpected") {
            
        $EmailSubject = "*Warning* - $ServerName - Unexpected Restart";
        Write-Information -Message "Setting Email Subject to:  $EmailSubject";
        Write-Log -Message "       Setting Email Subject to:  $EmailSubject";

		
        $EmailBody = "The event logs on $ServerName show that the previous system shutdown was unexpected after $Begin.";
        Write-Information -Message "Setting Email Body to: $EmailBody";
		
        Write-Information -Message "Sending email";
        Write-Log -Message "       Sending email";
        Write-Verbose -Message $BlankLine;
        Send-MailMessage -From $EmailFrom -To $EmailTo -Subject $EmailSubject -Body $EmailBody -SmtpServer $PSEmailServer;
        Write-Verbose -Message "Pausing script for 5 seconds to give time to send email.";
        Sleep 5
        Write-Information -Message "Email sent";
        Write-Log -Message "  Email sent";
        Write-Verbose -Message $BlankLine;
		return;
	} elseif ($RestartStatus -Match "expected") {
        
        $EmailSubject = "*Notice* - $ServerName - Restarted";
        Write-Information -Message "Setting Email Subject to:  $EmailSubject";
        Write-Log -Message "       Setting Email Subject to:  $EmailSubject";

        $EmailBody = "The event logs on $ServerName show that the server was restarted after $Begin.";
        Write-Information -Message "Setting Email Body to: $EmailBody";

        Write-Information -Message "Sending email.";
        Write-Log -Message "       Sending email.";
        Write-Verbose -Message $BlankLine;
        Send-MailMessage -From $EmailFrom -To $EmailTo -Subject $EmailSubject -Body $EmailBody -SmtpServer $PSEmailServer;
        Write-Verbose -Message "Pausing script for 5 seconds to give time to send email.";
        Sleep 5
        Write-Information -Message "Email sent.";
        Write-Log -Message "  Email sent.";
        Write-Verbose -Message $BlankLine;
        return;
    }
}

####################### Commands #######################

$Error.Clear()
Get-LogStatus
Write-Log -Message "*** Checking to see if last boot was a clean start. ***";
Write-Information -Message "*** Checking to see if last boot was a clean start. ***";
$RestartStatus = Check-SLogs
Send-AlertEmail $RestartStatus
Write-Log -Message "*** Clean Restart Script Completed ***"
Write-Information -Message "*** Clean Restart Script Completed ***";
$BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
Write-Information -Message $BlankLine;
$BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
Write-Information -Message $BlankLine;
$BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
Write-Information -Message $BlankLine;
Exit